"use strict";

function generateRandomPassword() {
  return Math.floor(Math.random() * 101);
}

const randomPassword = generateRandomPassword();
console.log(randomPassword);

let intentos = 1;
let intentosRestantes = 4;
let isPasswordCorrect = false;
let userNumber;

while (intentos <= 5 && userNumber != randomPassword) {
  let userNumberStr = prompt("Adivina la contraseña correcta entre 0 y 100: ");
  userNumber = parseInt(userNumberStr);
  console.log("Este es tu intento # ", intentos);
  if (userNumber > randomPassword && intentosRestantes > 0) {
    alert(
      `La contraseña es menor a ${userNumber}. Te quedan ${intentosRestantes} intentos, intentalo de nuevo`
    );
  } else if (userNumber < randomPassword && intentosRestantes > 0) {
    alert(
      `La contraseña es mayor a ${userNumber}. Te quedan ${intentosRestantes} intentos, intentalo de nuevo`
    );
  } else if (userNumber === randomPassword) {
    isPasswordCorrect = true;
  }

  intentosRestantes--;
  intentos++;
}

if (isPasswordCorrect) {
  alert(`¡Has ganado!, tu número ganador es: ${randomPassword}`);
} else {
  alert(`Has perdido, el número ganador era: ${randomPassword}`);
}
