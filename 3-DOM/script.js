"use strict";

function generateDivElement() {
  const clockContainer = document.createElement("div");
  const clockDisplay = document.createElement("h1");
  clockDisplay.setAttribute("id", "clockDisplay");

  clockContainer.append(clockDisplay);
  document.body.append(clockContainer);
}
generateDivElement();

function formatNum(num) {
  return num < 10 ? "0" + num : num;
}

function showTime() {
  let currentTime = new Date();
  let h = currentTime.getHours();
  let m = currentTime.getMinutes();
  let s = currentTime.getSeconds();

  let hour = formatNum(h);
  let min = formatNum(m);
  let sec = formatNum(s);
  let time = hour + ":" + min + ":" + sec;

  document.getElementById("clockDisplay").textContent = time;

  setTimeout(showTime, 1000);
}

showTime();
